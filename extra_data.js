// request_bulk_data_from_klear(data)
        // .then(data => {
        //     results = data
        //     var wb = XLSX.utils.book_new();
        
        //     var new_ws_name = "Infulencers";
            
        //     /* make worksheet */
        //     var ws_data = [
        //         [ "Username", "Influence", "Audience Size", 
        //         "Engagement Level", "Avg Like", "Avg Cmt", 
        //         "True Reach", "Avg Age", "DM Gender",
        //         "DM Ratio",
        //         "Avg Income", "Country 1st", "Co1 Ratto",
        //         "Country 2st", "Co2 Ratio", 
        //         "Country 3st", "Co3 Ratio", 
        //         "City 1st", "Ci1 Ratio", 
        //         "City 2st", "Ci2 Ratio", 
        //         "City 3st", "Ci3 Ratio",
        //         "Topic 1st", "To1 Ratio", 
        //         "Topic 2st", "To2 Ratio",
        //         "Topic 3st", "To3 Ratio",
        //         "Most popular hastags", "Most relevant hastags"],
        //     ];

        //     for (i = 0; i < results.length; i++) {
        //         ws_data.push(results[i])
        //     }

        //     var ws = XLSX.utils.aoa_to_sheet(ws_data);
            
        //     /* Add the worksheet to the workbook */
        //     XLSX.utils.book_append_sheet(wb, ws, new_ws_name);

        //     /* bookType can be any supported output type */
        //     var wopts = { bookType:'xlsx', bookSST:false, type:'array' };
        //     console.log(wb);
        //     var wbout = XLSX.writeFile(wb,'out.xlsx');
        //     res.send({status: 200, results: results})    
        // })


// async function alway return a promises
async function request_bulk_data_from_klear(data){
    var follows = data[0];
    var repost = data[1];

    /* fetch data IN SEQUENC */
    // var results = [];    
    // var follow_index = 0
    // for (follow_index=0; follow_index<follow.length; follow_index++) {
    //     var username = follow[follow_index]
    //     var respone = await fetch(`https://klear.com/instagram/${username}`)
    //     var body = await respone.text()
    //     results.push(extract_data(body))    
    // }

    /* fetch data IN PARALLEL */
    var results = await Promise.all(follows.map(async function(follow_user){
        var response = await fetch(`https://klear.com/instagram/${follow_user}`)
        var body = await response.text()
        return extract_data(follow_user, body)
    }))
    return results
}

function extract_data(follow_user, body){
    var $ = cheerio.load(body);
    
    var influence = 0
    var engagement_level = ''
    var average_like_number = 0
    var average_comment_number = 0
    var true_reach = 0
    var audience_size = 0
    var average_age = 0
    var dominant_gender = ''
    var dominant_gender_ratio = 0
    var average_income = 0
    var contry_1st = {name: '', ratio: ''}
    var contry_2st = {name: '', ratio: ''}
    var contry_3st = {name: '', ratio: ''}
    var city_1st = {name: '', ratio: ''}
    var city_2st = {name: '', ratio: ''}
    var city_3st = {name: '', ratio: ''}
    var topic_1st = {name: '', ratio: ''}
    var topic_2st = {name: '', ratio: ''}
    var topic_3st = {name: '', ratio: ''}
    var most_popular_hastags = ''
    var most_relevant_hastags = ''

    // get kpi top: influence, engagement level, true reach
    var kpi_tops = $('.boxMiddle .kpiTop')
    
    if(kpi_tops.length == 7) {
        influence = $(kpi_tops[0]).text().trim()
        engagement_level = $(kpi_tops[2]).text().trim()
        true_reach = $(kpi_tops[3]).text().trim()
        
        topic_1st.name = $(kpi_tops[4]).text().trim().replace(/[^a-zA-Z]+/g, '')
        topic_1st.ratio = $(kpi_tops[4]).text().trim().replace(/[^1-9.]+/g, '')+"%"
        topic_2st.name = $(kpi_tops[5]).text().trim().replace(/[^a-zA-Z]+/g, '')
        topic_2st.ratio = $(kpi_tops[5]).text().trim().replace(/[^1-9.]+/g, '')+"%"
        topic_3st.name = $(kpi_tops[6]).text().trim().replace(/[^a-zA-Z]+/g, '')
        topic_3st.ratio = $(kpi_tops[6]).text().trim().replace(/[^1-9.]+/g, '')+"%"
    }

    // get average like, comment number
    var like_comment_number_tag = $($('.rowOne .tightBoxThird')[1]).find('.kpiBottom .engagementsRow .kpiTitle');
    if (like_comment_number_tag.length == 4) {
        average_like_number = $(like_comment_number_tag[1]).text().trim()
        average_comment_number = $(like_comment_number_tag[3]).text().trim()
    }

    // get audience size
    var audience_size_tag = $('.num')
    audience_size = audience_size_tag !== 'undefined'? $(audience_size_tag).text().trim(): 0 
    
    // get average age, dominant gender
    var kpi_title_tags = $('.centerKpi .kpiTitle');
    var kpi_subtitle_tags = $('.centerKpi .kpiSubTitle');
    if(kpi_title_tags.length == 3) {
        average_age = $(kpi_title_tags[0]).text().trim()
        dominant_gender =  $(kpi_title_tags[1]).text().trim()
        dominant_gender_ratio =  $(kpi_subtitle_tags[1]).text().trim()
        average_income = $(kpi_title_tags[2]).text().trim()
    }

    var top_countries_cities_tags = $('.rowTwo .kpiRow')

    if(top_countries_cities_tags.length == 6) {
        contry_1st.name = $(top_countries_cities_tags[0]).find('.kpiTitle').text().trim()
        contry_1st.ratio = $(top_countries_cities_tags[0]).find('.kpiResult').text().trim()
        contry_2st.name = $(top_countries_cities_tags[1]).find('.kpiTitle').text().trim()
        contry_2st.ratio = $(top_countries_cities_tags[1]).find('.kpiResult').text().trim()
        contry_3st.name = $(top_countries_cities_tags[2]).find('.kpiTitle').text().trim()
        contry_3st.ratio = $(top_countries_cities_tags[2]).find('.kpiResult').text().trim()
        
        city_1st.name = $(top_countries_cities_tags[3]).find('.kpiTitle').text().trim()
        city_2st.ratio = $(top_countries_cities_tags[3]).find('.kpiResult').text().trim()
        city_3st.name = $(top_countries_cities_tags[4]).find('.kpiTitle').text().trim()
        city_1st.ratio = $(top_countries_cities_tags[4]).find('.kpiResult').text().trim()
        city_2st.name = $(top_countries_cities_tags[5]).find('.kpiTitle').text().trim()
        city_3st.ratio = $(top_countries_cities_tags[5]).find('.kpiResult').text().trim()
        
    }

    var most_popular_hastags_tag = $($('.wordBox')[0]).find('.kpiTitle')
    var most_relevant_hastags_tag = $($('.wordBox')[1]).find('.kpiTitle')

    most_popular_hastags = $(most_popular_hastags_tag).text()
    most_relevant_hastags = $(most_relevant_hastags_tag).text()

    // return {
    //     'username': follow_user, 
    //     'influence': influence,
    //     'audience_size': audience_size,
    //     'engagement_level': engagement_level,
    //     'average_like_number': average_like_number,
    //     'average_comment_number': average_comment_number,
    //     'true_reach': true_reach, 
    //     'average_age': average_age,
    //     'dominant_gender': dominant_gender+" | "+dominant_gender_ratio,
    //     'average_income': average_income,
    //     'country_1st': contry_1st.name+" | "+contry_1st.ratio,
    //     'country_2st': contry_2st.name+" | "+contry_2st.ratio,
    //     'country_3st': contry_3st.name+" | "+contry_3st.ratio,
    //     'city_1st': city_1st.name+" | "+city_1st.ratio,
    //     'city_2st': city_2st.name+" | "+city_2st.ratio,
    //     'city_3st': city_3st.name+" | "+city_3st.ratio,
    //     'topic_1st': topic_1st.name+" | "+topic_1st.ratio,
    //     'topic_2st': topic_2st.name+" | "+topic_2st.ratio,
    //     'topic_3st': topic_3st.name+" | "+topic_3st.ratio,
    //     'most_popular_hastags': most_popular_hastags,
    //     'most_relevant_hastags': most_relevant_hastags
    // }

    return [
        follow_user,
        influence,
        audience_size,
        engagement_level,
        average_like_number,
        average_comment_number,
        true_reach,
        average_age,
        dominant_gender,
        dominant_gender_ratio,
        average_income,
        contry_1st.name,
        contry_1st.ratio,
        contry_2st.name,
        contry_2st.ratio,
        contry_3st.name,
        contry_3st.ratio,
        city_1st.name,
        city_1st.ratio,
        city_2st.name,
        city_2st.ratio,
        city_3st.name,
        city_3st.ratio,
        topic_1st.name,
        topic_1st.ratio,
        topic_2st.name,
        topic_2st.ratio,
        topic_3st.name,
        topic_3st.ratio,
        most_popular_hastags,
        most_relevant_hastags
    ]
}