const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const XLSX = require('xlsx');
var multer  = require('multer')
var cheerio = require("cheerio")
var request = require("request")
var fetch = require("node-fetch")
var fs = require('fs');
var COLORS = require('./color')

//https://instagram.com/web/search/topsearch/?query=%23dogs
//https://www.instagram.com/explore/tags/dogs/?__a=1
const PRIVATE = 0
const NOT_PRIVATE = 1
const BOTH_PRIVATE_AND_NOT_PRIVATE = 2
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'))

const app = express()

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use("/js", express.static(path.join(__dirname, 'static/js')))
app.use("/images", express.static(path.join(__dirname, 'static/images')))
app.use("/css", express.static(path.join(__dirname, 'static/css')))

storage_dir = path.join(__dirname, 'static/uploads');
  const storage = multer.diskStorage({
    destination: storage_dir,
    filename: function(req, file, callback) {
        callback(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname));
    }
  })
  
  var upload_file = multer({
      storage: storage
  }).any()

app.all('*', function(req, res, next){
    var ip = req.ip.replace('::ffff:','')
    console.log(ip)
    if(config.ALLOWED_IPS.indexOf(ip) == -1) {
        console.log(ip)
        res.sendFile(path.join(__dirname, 'not_access.html'))
    } else {
        next()
    }
})


app.get('', function(req, res){
    res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/get-top-users', function(req, res, next){
    if(typeof req.body.hashtags == "undefined"
    || typeof req.body.filter_follower_count == "undefined"
    || typeof req.body.account_type == "undefined") {
        res.send({status: 500, err: 'Missing parameters'})
    }
    var hashtags_str = req.body.hashtags;
    var filter_follower_count = req.body.filter_follower_count
    var account_type = req.body.account_type
    var filter = req.body.filter
    
    get_top_users(hashtags_str.split("\n"), filter_follower_count, account_type, filter)
    .then(top_users => {
        var wb = XLSX.utils.book_new();
        
        var new_ws_name = "Top Users";
        ws_data = extra_ws_data_users(top_users)
        
        var ws = XLSX.utils.aoa_to_sheet(ws_data);
        
        /* Add the worksheet to the workbook */
        XLSX.utils.book_append_sheet(wb, ws, new_ws_name);

        /* bookType can be any supported output type */
        var wopts = { bookType:'xlsx', bookSST:false, type:'array' };
        // console.log(wb);
        var wbout = XLSX.writeFile(wb,'top_users.xlsx');

        res.send({status: 200, data: top_users}) 
    }).catch(err => {
        print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
        console.log(err)
    })
})

app.get('/download-top-users', function(req, res){
    res.download(__dirname + '/top_users.xlsx')
})

app.post('/get-result', upload_file, function (req, res, next) {
    // console.log(req.files)
    if(typeof req.files == "undefined" || req.files.length == 0) {
        res.send({status: 500, error: 'No file seleted'})
    } else {
        var workbook = XLSX.readFile(req.files[0].path);
        var first_sheet_name = workbook.SheetNames[0];
        var address_of_cell = 'A1';
        
        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];
        
        /* Find !ref */
        var ref_value = worksheet['!ref'].v
        var range = XLSX.utils.decode_range(worksheet['!ref']);
        data = [];
        for (colNum=0; colNum<=1; colNum++){
            col = [];
            for(rowNum = 1; rowNum <= range.e.r; rowNum++){
                var nextCell = worksheet[
                    XLSX.utils.encode_cell({r: rowNum, c: colNum})
                ];
                if( typeof nextCell !== 'undefined' ) col.push(nextCell.v);
            }
            data.push(col)    
        }
        
        
        get_bulk_data_from_klear_api(data).then(data => {
            var wb = XLSX.utils.book_new();
            
            var new_ws_name = "Infulencers";
            ws_data = extra_ws_data(data)
            
            var ws = XLSX.utils.aoa_to_sheet(ws_data);
            
            /* Add the worksheet to the workbook */
            XLSX.utils.book_append_sheet(wb, ws, new_ws_name);

            /* bookType can be any supported output type */
            var wopts = { bookType:'xlsx', bookSST:false, type:'array' };
            // console.log(wb);
            var wbout = XLSX.writeFile(wb,'insta_insight.xlsx');
            fs.unlink(req.files[0].path, (err) => {
                if (err) {
                    print_text_color(COLORS.FgGreen, 'CATCH ERROR ....')
                    console.log(err)
                }
                console.log("Remove file success")
            });
            res.send({status: 200})
        }).catch(err => {
            print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
            console.log(err)
            res.send({status: 500})
        })

    }
})

app.get('/download', function(req, res){
    res.download(__dirname + '/insta_insight.xlsx')
})

app.listen(8080, function(){
    console.log("Server is running at localhost:8080...")
})

function print_text_color(color, text){
    console.log('%s%s%s', color, text, COLORS.Reset)
}

async function get_bulk_data_from_klear_api(data){
    var follows = data[0];
    _cookie = config.cookie
    if(_cookie == '') return false;
    const opts = {
        headers: {
            cookie: _cookie
        }
    };
    var results = await Promise.all(follows.map(async function(follow_user, index){
        var data_demographics = 0
        var data_demographics_aditional = 0 
        var data_influence = 0
        
        try {
            /* get demographics data */
            const response_demographics = await fetch(`https://klear.com/api/2.1/Profile/instagram/${follow_user}/overview/demographics`, opts);
            const result_demographics = await response_demographics.json()
            if(typeof result_demographics !== 'undefined') {
                // console.log(result_demographics)
                if(result_demographics.hasOwnProperty('instagram')) {
                    data_demographics = result_demographics.instagram.demographics;        
                }
            }
        } catch(err) {
            data_demographics = 0
        }

        try {
            /* get influence data */
            const response_influence = await fetch(`https://klear.com/api/2.1/Profile/instagram/${follow_user}/overview/influence`, opts);
            const result_influence = await response_influence.json()
            if(typeof result_influence !== 'undefined') {
                // console.log(result_influence)
                if(result_influence.hasOwnProperty('influence') && result_influence.influence != false) {
                    data_influence = result_influence        
                }
            }
        } catch(err) {
            print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
            console.log(err)
            data_influence = 0
        }

        try {
            /* get demographics aditional */
            const response_demographics_aditional = await fetch(`https://klear.com/api/2.1/Profile/instagram/${follow_user}/Overview/demographics-additional`, opts);
            const result_demographics_aditional = await response_demographics_aditional.json()
            if(typeof result_demographics_aditional.instagramInteractionsHashtags !== 'undefined') {
                // console.log(result_demographics_aditional)
                if(result_demographics_aditional.instagramInteractionsHashtags.hasOwnProperty('popular')) {
                    data_demographics_aditional = result_demographics_aditional        
                }
            }
        } catch(err) {
            print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
            console.log(err)
            data_demographics_aditional = 0
        }
        console.log(`[${index}] DONE get insight user: ${follow_user}`)
        return {
            "username": follow_user,
            "data_demographics": data_demographics,
            "data_demographics_aditional": data_demographics_aditional,
            "data_influence": data_influence
        }
    }))
    return results
}


function extra_ws_data(results){
    ws_data = [
        [
            "Username",

            "Audien Size",
            "Influence",
            //"Engagement Level",
            "Avg Like",
            "Avg Cmt",
            "True Reach",

            
            "Men Ratio",
            "Women Ratio",
            "Age 12-17",
            "Age 18-24",
            "Age 25-34",
            "Age 35-49",
            "Age 50-64",
            "Age 64+",

            "Avg Income", 
            "Co1", 
            "Co1 Ratto",
            "Co2", 
            "Co2 Ratio", 
            "Co3", 
            "Co3 Ratio", 
            "Ci1", 
            "Ci1 Ratio", 
            "Ci2", 
            "Ci2 Ratio", 
            "Ci3", 
            "Ci3 Ratio",
            "To1", 
            "To1 Ratio", 
            "To2", 
            "To2 Ratio",
            "To3", 
            "To3 Ratio",
            
            "Most popular hastags", 
            "Most relevant hastags"
        ]
    ]

    for (let index=0; index<results.length; index++) {
        let origial_influencer_data = results[index]
        var influencer_data = [origial_influencer_data.username]
        // check if klear has data about influencer
        if(origial_influencer_data.data_influence != 0) {
            influencer_data.push(origial_influencer_data.data_influence.networkStats.insta.followers_long)            
            influencer_data.push(origial_influencer_data.data_influence.influence.influence)
            influencer_data.push(origial_influencer_data.data_influence.engagements.instagram.likes_per_photo)
            influencer_data.push(origial_influencer_data.data_influence.engagements.instagram.comments_per_photo)
            influencer_data.push(origial_influencer_data.data_influence.highlights.reach.instagram.global)
        } else {
            for(let i=0; i<5; i++) {
                influencer_data.push(0)
            }
        }

        // check if klear has data about influencer demographics
        if(origial_influencer_data.data_demographics != 0) {
            // men, women ratio
            if(origial_influencer_data.data_demographics.genders != null && typeof origial_influencer_data.data_demographics.genders !== 'undefined'){
                influencer_data.push(origial_influencer_data.data_demographics.genders["1"])
                influencer_data.push(origial_influencer_data.data_demographics.genders["2"])        
            } else {
                influencer_data.push(0)
                influencer_data.push(0)
            }
            
            // age
            if(origial_influencer_data.data_demographics.ages != null && typeof origial_influencer_data.data_demographics.ages !== 'undefined') {
               let ages = origial_influencer_data.data_demographics.ages;
               if(ages.hasOwnProperty("12 - 17")) {
                    influencer_data.push(ages["12 - 17"])
                } else {
                    influencer_data.push(0)
                }

                if(ages.hasOwnProperty("18 - 24")) {
                    influencer_data.push(ages["18 - 24"])
                } else {
                    influencer_data.push(0)
                }

                if(ages.hasOwnProperty("25 - 34")) {
                    influencer_data.push(ages["25 - 34"])
                } else {
                    influencer_data.push(0)
                }

                if(ages.hasOwnProperty("35 - 49")) {
                    influencer_data.push(ages["35 - 49"])
                } else {
                    influencer_data.push(0)
                }

                if(ages.hasOwnProperty("50 - 64")) {
                    influencer_data.push(ages["50 - 64"])
                } else {
                    influencer_data.push(0)
                }

                if(ages.hasOwnProperty("65+")) {
                    influencer_data.push(ages["65+"])
                } else {
                    influencer_data.push(0)
                }
            } else {
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
            }
            

            // income
            influencer_data.push(origial_influencer_data.data_demographics.incomeLevels.summary.average)
            
            // top country, city
            if (origial_influencer_data.data_demographics.countries == null 
                || origial_influencer_data.data_demographics.countries.length == 0) {
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
            } else {
                let countries = origial_influencer_data.data_demographics.countries;
                // 1st country
                if(typeof countries[0] !== 'undefined') {
                    influencer_data.push(countries[0].countryName)    
                    influencer_data.push(countries[0].countryPercentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

                // 2st country
                if(typeof countries[1] !== 'undefined') {
                    influencer_data.push(countries[1].countryName)    
                    influencer_data.push(countries[1].countryPercentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

                // 3st country
                if(typeof countries[2] !== 'undefined') {
                    influencer_data.push(countries[2].countryName)    
                    influencer_data.push(countries[2].countryPercentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

            }

            // top city
            if(origial_influencer_data.data_demographics.cities == null
            || origial_influencer_data.data_demographics.cities.length == 0) {
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
            } else {
                let cities = origial_influencer_data.data_demographics.cities;
                // 1st city
                if(typeof cities[0] !== 'undefined') {
                    influencer_data.push(cities[0].cityName)    
                    influencer_data.push(cities[0].cityPercentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

                // 2st city
                if(typeof cities[1] !== 'undefined') {
                    influencer_data.push(cities[1].cityName)    
                    influencer_data.push(cities[1].cityPercentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

                // 3st city
                if(typeof cities[2] !== 'undefined') {
                    influencer_data.push(cities[2].cityName)    
                    influencer_data.push(cities[2].cityPercentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

            } 

            // top topic 
            if(origial_influencer_data.data_demographics.skills == null
            || origial_influencer_data.data_demographics.skills.length == 0) {
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
                influencer_data.push(0)
            } else {
                let skills = origial_influencer_data.data_demographics.skills;
                // 1st topic
                if(typeof skills[0] !== 'undefined') {
                    influencer_data.push(skills[0].skill)    
                    influencer_data.push(skills[0].percentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

                // 2st topic
                if(typeof skills[1] !== 'undefined') {
                    influencer_data.push(skills[1].skill)    
                    influencer_data.push(skills[1].percentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

                // 3st topic
                if(typeof skills[2] !== 'undefined') {
                    influencer_data.push(skills[2].skill)    
                    influencer_data.push(skills[2].percentage)    
                } else {
                    influencer_data.push(0)
                    influencer_data.push(0)    
                }

            } 


        } else {
            for(let i=0; i<27; i++) {
                influencer_data.push(0)
            }
        }

        // check if klear has data about influencer demographics aditional

        if(origial_influencer_data.data_demographics_aditional != 0) {
            // popular hashtags
            if(origial_influencer_data.data_demographics_aditional.instagramInteractionsHashtags.popular.length > 0){
                let popular_hashtags = origial_influencer_data.data_demographics_aditional.instagramInteractionsHashtags.popular.map(function(hashtag_obj){
                    return `#${hashtag_obj.name}`
                });
                influencer_data.push(popular_hashtags.join(' '))
            } else {
                influencer_data.push('')
            }

            // dominant hashtags
            if(origial_influencer_data.data_demographics_aditional.instagramInteractionsHashtags.dominant.length > 0){
                let dominant_hashtags = origial_influencer_data.data_demographics_aditional.instagramInteractionsHashtags.dominant.map(function(hashtag_obj){
                    return `#${hashtag_obj.name}`
                });
                influencer_data.push(dominant_hashtags.join(' '))
            } else {
                influencer_data.push('')
            }

        } else {
            for(let i=0; i<2; i++) {
                influencer_data.push(0)
            }
        }
        
        ws_data.push(influencer_data)
    }
    return ws_data
}

Array.prototype.myIndexOf = function(f)
{
    for(var i=0; i<this.length; i++)
    {
        if( f(this[i]) )
            return i;
    }
    return -1;
};

async function get_top_users(hashtags, filter_follower_count, filter){
    var __cookie = `ds_user_id=7324433888; shbid=2846; csrftoken=SqX7Rj1OEAUk1yhFxeqKDTPYEpQXZ6nS; mid=Wvo0PQAEAAHPpH1T7TTMVnjYyHgx; rur=ATN; csrftoken=SqX7Rj1OEAUk1yhFxeqKDTPYEpQXZ6nS; SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1; fbm_124024574287414=base_domain=.instagram.com; fbsr_124024574287414=TEkNiqRAQV_XwfriOv6SuGkszLk-ey-EmDK3bEPYyxg.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUItOXJKMm1DMU1MMzRGci1WWGd6OHFvVEZpRG5QQlR1cDBwU1doRGhJYWtlZERxZzNBY0xVNl9Oc0lsS3hrMDF3d3RsZE1PbDdHN1l6ak82bTc4aDlpcHJ4a0hkUUwxbW9tY3F2dExfQ0Z0c0hKTk8wRGtoZFpaT0xFc1o2U3IwVC1TbUhhQmhoTHMyQTFOWTdKMHVqT1RiU05OWXpsREJGWk1WWHJ6M2c3Nl9Maktqa3cyNmZCTElfbE5EUFZkSE1oSHBXRlBlT1d5SWtmZHRjX2lNd25GQmtLaVZnal9HakVGUFBYWkhzRFhfZUw1XzBKeGFSaXFFUktnOHJRWTZXNnRhdTcwaWVkUi1JemVCTmhDTzZ2dXpkdlhScjdnNVZPNFFfaE9WQktET2oxVTFRdVpBNlp6M0VWRXJrcjJGX0pncUFqczhRU2h5QnR5clBTeFZkdSIsImlzc3VlZF9hdCI6MTUyNjM1NzA4NSwidXNlcl9pZCI6IjEwMDAwMzg1Mjg3MzE0OSJ9; urlgen="{\"time\": 1526355192}:1fIS65:pqc68LhkfZUUjg_nK3kxNDsOrVc"`
    const opts = {
        headers: {
            cookie: __cookie
        }
    };
    /* trim space */
    var hashtags_to_get_top_users = hashtags.map(function(hashtag){
        return hashtag.trim()
    })
    /* get hashtag from text area */
    hashtags_to_get_top_users = hashtags_to_get_top_users.filter(function(hashtag) { 
        return hashtag !== '' && (typeof hashtag !== undefined)
    });
    var index_hashtag = 0;
    /* get top post of hashtag */
    var results = await Promise.all(hashtags_to_get_top_users.map(async function(hashtag){
        try {
            hashtag = hashtag.replace(/\#/g, '')
            var response = await fetch(`https://www.instagram.com/explore/tags/${hashtag}/?__a=1`, opts)
            var result = await response.json()
            // while(result.status == 'fail') {
            //     response = await fetch(`https://www.instagram.com/explore/tags/${hashtag}/?__a=1`)
            //     result = await response.json()
            // }
            index_hashtag++
            console.log(`[${index_hashtag}] DONE top_post of hashtag ${hashtag}`)
            return result
        } catch(err) {
            print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
            console.log(err)
            return undefined
        }
        
    }))

    results = results.filter(function(result){
        return (typeof result !== "undefined") && result.hasOwnProperty('graphql')
    })

    // console.log(results)

    /* get shortcode of each post */
    var top_user_ids = [];
    var top_shortcodes = [];
    
    results.map(function(result){
        let top_9_posts = result.graphql.hashtag.edge_hashtag_to_top_posts.edges
        let top_9_user_ids = top_9_posts.map(function(post){
            return post.node.owner.id
        })
        
        let top_9_shortcodes = top_9_posts.map(function(post){
            return post.node.shortcode
        })
        
        top_9_user_ids.map(function(user_id){
            if (top_user_ids.indexOf(user_id) == -1) top_user_ids.push(user_id)
        })

        top_9_shortcodes.map(function(shortcode){
            if (top_shortcodes.indexOf(shortcode) == -1) top_shortcodes.push(shortcode)
        })
        
    })

    /* get top users without remove duplicate */
    var index_original_top_users = 0
    var original_top_users = []
    original_top_users = await Promise.all(top_shortcodes.map(async function(shortcode){
        try {
            var response = await fetch(`https://www.instagram.com/p/${shortcode}/?__a=1`, opts)
            var result = await response.json()
            // while(result !== undefined && result.status == 'fail') {
            //     response = await fetch(`https://www.instagram.com/p/${shortcode}/?__a=1`)
            //     result = await response.json()
            // }
            if (typeof result !== "undefined" && result.hasOwnProperty('graphql')) {
                // return result.graphql.shortcode_media.owner.username
                index_original_top_users++
                console.log(`[${index_original_top_users}] username: ${result.graphql.shortcode_media.owner.username}`)
                return {
                    'id': result.graphql.shortcode_media.owner.id,
                    'username': result.graphql.shortcode_media.owner.username,
                    'follower_count': -1,
                    'full_name': '',
                    'is_private': '',
                    'is_verified': '',
                    'profile_url': `https://www.instagram.com/${result.graphql.shortcode_media.owner.username}`
                }
            }
        } catch(err){
            print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
            console.log(err)
            return undefined
        }
    }))

    /* get top users after remove duplicate */
    var top_users = original_top_users.filter(function(user, index, self){
        // return index == self.indexOf(user)
        if(typeof user == 'undefined') return false
        return index == self.myIndexOf(function(item){
            if (typeof item == 'undefined') return false
            return item.id == user.id && item.username == user.username
        })
    })
    console.log(top_users)
    // if don't select filter -> return list username
    if(filter == 0)
        return top_users

    // else filter top user
    var index_top_users_without_filter = 0
    var top_users_without_filter = await Promise.all(top_users.map(async function(user){
        // var default_result = {
        //     'id': user.id,
        //     'username': user.username,
        //     'follower_count': -1,
        //     'full_name': '',
        //     'is_private': '',
        //     'is_verified': '',
        //     'profile_url': `https://www.instagram.com/${user.username}`
        // } 
        try {
            var response = await fetch(`https://www.instagram.com/web/search/topsearch/?query=${user.username}`, opts)
            var result = await response.json()
            if( typeof result != 'undefined' && result.status == 'ok') {
                if(result.users.length > 0 && result.users[0].user.username == user.username) return {
                    'id': user.id,
                    'username': user.username,
                    'follower_count': result.users[0].user.follower_count,
                    'full_name': result.users[0].user.full_name,
                    'is_private': result.users[0].user.is_private,
                    'is_verified': result.users[0].user.is_verified,
                    'profile_url': `https://www.instagram.com/${user.username}`
                } 
            } else {
                // return default_result
                return user
            }
            index_top_users_without_filter++
            console.log(`[${index_top_users_without_filter}] DONE get user info of ${user.username}`)                        
        } catch(err) {
            print_text_color(COLORS.FgGreen, 'CATCH EXCEPTION ....')
            console.log(err)
            return default_result
        }
            
    }))
    
    top_users_without_filter = top_users_without_filter.filter(function(user){
        return typeof user !== "undefined"
    })


    // console.log(top_users_without_filter)
    /* Filter users */
    
    var top_users_filtered = top_users_without_filter.filter(function(user){
        // CHECK IF DID NOT GETTED FOLLOER, IS_PRIVATE INFO
        if(user.follower_count == -1) return true

        var satify_filter_follower_count = true

        if(filter_follower_count > 0 && user.follower_count < filter_follower_count) {
            satify_filter_follower_count = false
        }

        if(account_type == BOTH_PRIVATE_AND_NOT_PRIVATE) {
            // dont care private or not
            return true && satify_filter_follower_count
        } else if(account_type == PRIVATE) {
            return user.is_private == true && satify_filter_follower_count
        } else if (account_type == NOT_PRIVATE) {
            return user.is_private == false && satify_filter_follower_count            
        }
    })
    // console.log(top_users_filtered)
    return top_users_filtered
    
}

function sleep(seconds){
    setTimeout(function(){}, seconds*1000)
}

function extra_ws_data_users(top_users) {
    ws_data = [
        [
            "Username",
            "Follower Count",
            "Profile Url",
            "Full Name",
            "Is Private",
            "Is Verified",
            "ID"
        ]
    ]
    top_users.map(function(user){
        ws_data.push([
            user.username,
            user.follower_count,
            user.profile_url,
            user.full_name,
            user.is_private,
            user.is_verified,
            user.id
        ])
    })
    return ws_data
}