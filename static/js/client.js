$(document).ready(function(){
    $('.btn-send').click(function(){
        var form_data = new FormData();
        var file = ($('input.file-top-users')[0].files)[0]
        form_data.append('file', file)
        $.ajax({
            type: 'post',
            url: '/get-result',
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $('.overlay').css({'display': 'block'})
            },
            success: function(res){
                console.log(res);
                $('.overlay').css({'display': 'none'})                
                if(res.status == 200) {
                    $('.download-result').css({'display': 'block'})            
                } else {
                    alert("Oopps. Something wrong")
                }
            },
            error: function(err) {
                console.log(err)
                alert("Oopps. Something wrong")
            }
        })
    })

    $('.btn-reset-search').click(function(){
        $('.search-hashtag-result').val('')
    })

    $('.btn-search-hashtag').click(function(){
        var keywords = $('.search-hashtag-keyword').val().split('\n');
        keywords = keywords.map(item => item.trim())
        console.log(keywords)        
        $.each(keywords, function(index, keyword){
            console.log(keyword)
            $.ajax({
                type: 'get',
                url: `https://instagram.com/web/search/topsearch/?query=%23${keyword}`,
                success: function(res){
                    if(res.hashtags !== undefined) {
                        var hashtags = $('.search-hashtag-result').val().split('\n');
    
                        var filter_number_post = parseInt($('.filter-number-post').val())
                        let result_with_hash = '';
                        let result_without_hash = '';
    
                        res.hashtags.map(function(hashtag_obj){
                            if(hashtag_obj.hashtag.media_count >= filter_number_post) {
                                hashtags.push(`#${hashtag_obj.hashtag.name}`)
                            }
                        })
    
                        hashtags = hashtags.filter(function(hashtag, index, self){
                            return hashtag != '#' && index == self.indexOf(hashtag) && hashtag !== undefined
                        })
    
                        hashtags.map(function(hashtag){
                            result_with_hash += `${hashtag}\r\n`
                        })
                        
                        $('.search-hashtag-result').val(result_with_hash)
                        $('.hashtags-to-get-top-users').val(result_with_hash)
                    }
                }
            })
        })
    })

    $('.btn-get-top-users').click(function(){
        var filter = 1
        if($(this).attr('id') == 'btn-get-top-users-only-username') {
            filter = 0
        }
        var hashtags = $('.hashtags-to-get-top-users').val();
        var filter_follower_count = $('.filter-follower-count').val()
        var account_type = parseInt($('input[name=acc-type]:checked').val())
        
        filter_follower_count = filter_follower_count == ''? -1: parseInt(filter_follower_count)

        $.ajax({
            type: 'post',
            url: '/get-top-users',
            data: {
                'hashtags': hashtags,
                'filter_follower_count': filter_follower_count,
                'account_type': account_type,
                'filter': filter
            },
            success: function(res){
                console.log(res)
                $('.overlay').css({'display': 'none'})                
                if(res.status == 200) {
                    $('.download-top-users').css({'display': 'block'})            
                } else {
                    alert("Oopps. Something wrong")
                }
            },
            error: function(err){
                console.log(err)
            },
            beforeSend: function(){
                $('.overlay').css({'display': 'block'})
            }
        })
        // check user follower 
        // https://www.instagram.com/web/search/topsearch/?query=sydney_my_kooikerlove      
    })
})
